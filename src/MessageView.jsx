import './ErrorMessage.css';
import {clsx} from "clsx";

export default function MessageView({message}) {
    const classes = clsx({
        "msg-view": true,
        "error-msg": message.isError,
        "info-msg": !message.isError,
        "hidden-msg": !message.text
    });
    return (
        <span className={classes}>
            {message.text}
        </span>
    );
}