import './Form.css';
import {useState} from "react";
import Identity from "./Identity.jsx";
import {clsx} from "clsx";

export default function RegistrationForm({setResponse, setErrorMsg}) {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [type, setType] = useState('email');
    const [identity, setIdentity] = useState('');
    const [password, setPassword] = useState('');
    const [btnDisabled, setBtnDisabled] = useState(false);

    const handleChangeName = (event, setter) => {
        setter(event.target.value.replace(/\W|\d/, ''));
    }
    const handleSubmitClick = e => {
        e.preventDefault();
        const body = {firstName, lastName, type, identity, password};
        const errorMsg = 'Password does not meet the requirements';
        const emptyMsg = '';
        const isOk = true;
        if (isOk) {
            setResponse({identity});
            setBtnDisabled(false);
            setErrorMsg({ isError: false, text: 'Verify your registration' })
        } else {
            setBtnDisabled(true);
            setErrorMsg({ isError: true, text: errorMsg });
            setTimeout(() => {
                setBtnDisabled(false);
                setErrorMsg(emptyMsg);
            }, 6000);
        }
    }

    const btnClasses = clsx({"submit-btn": true, "disabled-btn": btnDisabled});

    return (
        <form className='register-form' action="" onSubmit={handleSubmitClick}>
            <input className='text-field' type="text"
                   value={firstName} placeholder='Zafod'
                   onChange={e => handleChangeName(e, setFirstName)}/>
            <input className='text-field' type="text"
                   value={lastName} placeholder='Beeblebrocks'
                   onChange={e => handleChangeName(e, setLastName)}/>
            <Identity type={type} setType={setType} identity={identity} setIdentity={setIdentity}/>
            <input className='text-field' type="password"
                   value={password} placeholder='your password'
                   onChange={e => setPassword(e.target.value)}/>
            <input className={btnClasses} type="submit" value='register'/>
        </form>
    );
}