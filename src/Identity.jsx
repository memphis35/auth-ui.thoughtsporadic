import './Identity.css';
import phone from './assets/phone.svg';
import email from './assets/email.svg';

import {useEffect} from "react";

export default function Identity({type, setType, identity, setIdentity}) {
    useEffect(() => setType(type), []);
    const handleTypeChange = e => {
        setType(e.target.value);
        setIdentity(e.target.value === 'phone' ? '+' : '');
    };
    const handleEmailIdentityChange = event => {
        const a = event.target.value.replace(/[^a-z0-9_.-@]/, '');
        setIdentity(a);
    }
    const handlePhoneIdentityChange = event => {
        const origin = event.target.value;
        if (!origin) {
            setIdentity('+');
        } else {
            console.log(origin);
            const replaced = origin.substring(1).replace(/[^0-9]/, '');
            console.log(replaced);
            setIdentity('+' + replaced);
        }
    };
    const isEmail = type === 'email';
    const handleSwitchMethod = event => {
        event.currentTarget.classList.toggle('rotate-select')
        setType(isEmail ? 'phone' : 'email');
        setIdentity('');
    }
    return (
        <div className='method-field'>
            <button className='select-field' type='button' onClick={handleSwitchMethod}>
                <img className='select-icon' src={isEmail ? email : phone} alt={type}/>
            </button>
            <input className='identity-field'
                   type='text'
                   value={identity}
                   placeholder={isEmail ? 'zafod.beeblebrocks@mail.com' : '+79211234567'}
                   onChange={isEmail ? handleEmailIdentityChange : handlePhoneIdentityChange}
            />
        </div>);
}