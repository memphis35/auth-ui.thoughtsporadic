import SwitchButton from './SwitchButton.jsx';
import RegistrationForm from "./RegistrationForm.jsx";
import LoginForm from "./LoginForm.jsx";
import MessageView from "./MessageView.jsx";

import './App.css';
import logo from './assets/logo.png'

import {useState} from "react";
import VerificationForm from "./VerificationForm.jsx";

export default function App() {
    const [currentForm, setCurrentForm] = useState('register');
    const [response, setResponse] = useState(undefined);
    const [message, setMessage] = useState({});

    const toggleForm = event => {
        setResponse(undefined);
        setCurrentForm(event.target.innerText.toLowerCase());
    };
    return (
        <div className='main-window'>
            <img className='logo' src={logo} alt="logo"/>
            <SwitchButton currentForm={currentForm} toggleForm={toggleForm}/>
            {currentForm === 'register' && <RegistrationForm setResponse={setResponse} setErrorMsg={setMessage}/>}
            {currentForm === 'login' && <LoginForm setErrorMsg={setMessage}/>}
            <VerificationForm response={response} setMessage={setMessage} message={message}/>
            <MessageView message={message}/>
        </div>
    );
}
