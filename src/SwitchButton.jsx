import './SwitchButton.css';
import {clsx} from "clsx";

export default function SwitchButton({currentForm, toggleForm}) {
    const isRegister = currentForm === 'register';
    const registerStyles = clsx({"switch-form-btn": true, "switched-btn": isRegister});
    const loginStyles = clsx({"switch-form-btn": true, "switched-btn": !isRegister});
    return (
        <div className='switch-button'>
            <button className={registerStyles} onClick={toggleForm} disabled={isRegister}>register</button>
            <span className='delimiter'></span>
            <button className={loginStyles} onClick={toggleForm} disabled={!isRegister}>login</button>
        </div>
    );
}