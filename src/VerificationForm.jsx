import './Form.css';

import {useState} from "react";
import {clsx} from "clsx";

export default function VerificationForm({response, setMessage, message}) {
    const [code, setCode] = useState('');
    const [btnDisabled, setBtnDisabled] = useState(false);
    const handleCodeChange = event => {
        const origin = event.target.value;
        const replaced = origin.replace(/[^0-9]+/, '').substring(0, 6);
        setCode(replaced);
    };
    const btnClasses = clsx({
        "verify-btn": true,
        "disabled-btn": code.length !== 6
    });
    const formClasses = clsx({
        "verification-form": true,
        "hidden-form": !response
    });
    const handleFormSubmit = e => {
        e.preventDefault();
        const id = response.identity;
        const body = {id, code};
        console.log(body);
        window.location.replace('account');
    };
    return(
        <form className={formClasses} action="/verify" onSubmit={handleFormSubmit}>
            <input className='code-field' type="text" onChange={handleCodeChange} placeholder='123456' value={code}/>
            <input className={btnClasses} type="submit" value='check' disabled={btnDisabled || code.length !== 6}/>
        </form>
    );
}