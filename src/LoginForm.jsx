import './Form.css';
import Identity from "./Identity.jsx";
import {useState} from "react";

export default function LoginForm() {
    const [type, setType] = useState('email');
    const [identity, setIdentity] = useState('');
    const [password, setPassword] = useState('');
    const handleSubmitClick = e => {
        e.preventDefault();
        const body = { type, identity, password };
        console.log(body);
    }
    return(
        <form className='login-form' onSubmit={handleSubmitClick}>
            <Identity type={type} setType={setType} identity={identity} setIdentity={setIdentity}/>
            <input className='text-field' type="password"
                   value={password} placeholder='your password'
                   onChange={e => setPassword(e.target.value)}
            />
            <input className='submit-btn' type="submit" value='login'/>
        </form>
    );
}